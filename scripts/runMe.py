import sys
from glob import glob

import numpy as np
import cv2
import ast
import os
import pandas as pd
from enum import Enum

class color(Enum):
    nanColor = 0
    green = 1
    yellow = 2
    white = 3
    gray = 4
    blue = 5
    red = 6

class bus:
    def __init__(self):
        self.color = color.nanColor
        self.xmin = 0
        self.ymin = 0
        self.width = 0
        self.height = 0

class imagePreprocess:
    def __init__(self, path):
        self.iamge_path = path
        self.object = cv2.imread(path)
        self.width = 840
        self.high = 640
        dim = (self.width, self.high)
        self.image_object = cv2.resize(cv2.imread(path),dim)
        self.image_object1 = self.image_object

    def createADataBase(self):
        return

    def wheel_find(self):
        return


def dirReader(buses_path):
    print(os.getcwd())
    file_list = glob(buses_path+"\\*.JPG")
    for file in file_list:
        imagePreprocess(os.path.abspath(file))
    return


def run(myAnnFileName, buses):
    annFileNameGT = os.path.join(os.getcwd(),'annotationsTrain.txt')
    writtenAnnsLines = {}
    annFileEstimations = open(myAnnFileName, 'w+')
    annFileGT = open(annFileNameGT, 'r')
    writtenAnnsLines['Ground_Truth'] = (annFileGT.readlines())
    dirReader(buses)
    for k, line_ in enumerate(writtenAnnsLines['Ground_Truth']):

        line = line_.replace(' ','')
        imName = line.split(':')[0]
        anns_ = line[line.index(':') + 1:].replace('\n', '')
        anns = ast.literal_eval(anns_)
        if (not isinstance(anns, tuple)):
            anns = [anns]
        corruptAnn = [np.round(np.array(x) + np.random.randint(low = 0, high = 100, size = 5)) for x in anns]
        corruptAnn = [x[:4].tolist() + [anns[i][4]] for i,x in enumerate(corruptAnn)]
        strToWrite = imName + ':'
        if(3 <= k <= 5):
            strToWrite += '\n'
        else:
            for i, ann in enumerate(corruptAnn):
                posStr = [str(x) for x in ann]
                posStr = ','.join(posStr)
                strToWrite += '[' + posStr + ']'
                if (i == int(len(anns)) - 1):
                    strToWrite += '\n'
                else:
                    strToWrite += ','
        annFileEstimations.write(strToWrite)


if __name__ == '__main__':
    i = 0
    val = {}
    while(i < len(sys.argv)):
        arg = sys.argv[i]
        if(arg == "-answer"):
            val['answer'] = sys.argv[i+1]
        elif(arg == "-busses"):
            val['busses'] = sys.argv[i + 1]
        elif(arg == "-annotation"):
            val['annotation'] = sys.argv[i + 1]
        i = i + 1
    dataBase = parseFile(val['annotation'])
    dataBasePD = pd.DataFrame(dataBase)
    createImageDataBase(dataBasePD,val['busses'])
    pd.DataFrame(dataBase).to_csv("result.csv")
    exit(1)
    run(val['answer'],val['busses'])

