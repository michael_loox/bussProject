import time
from glob import glob
import matplotlib.pyplot as plt
import cv2
import sys
import os

import numpy as np
import pandas as pd
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, \
    img_to_array, array_to_img
import keras
#from keras.preprocessing.image import ImageDataGenerator



class timeer:

    def tic(self):
        self.t = time.time()

    def toc(self):
        self.elapsed = float(time.time()) - float(self.t)
        s = "elapsed time is %0.3f seconds" % self.elapsed
        print(s)
        return self.elapsed


def setUpDircoty(path):
    for i in range(1,7):
        dirName = path+str(i)
        try:
            # Create target Directory
            os.makedirs(dirName)
            print("Directory ", dirName, " Created ")
        except FileExistsError:
            print("Directory ", dirName, " already exists")
    return

def parseFile(path):
    dataBase = {
                'image':[],
                'xmin':[],
                'ymin':[],
                'width':[],
                'higth':[],
                'busNumber':[],
               }
    file_list = open(path,'r').readlines()
    for line in file_list:
        row = line.split(":")
        image_name = row[0]
        line_to_process=row[1].replace(" ","").replace("[","").replace("]",
                                                                   "").strip("\n").split(",")
        for i in range(len(line_to_process),0,-5):
            dataBase['busNumber'].append(line_to_process.pop())
            dataBase['higth'].append(line_to_process.pop())
            dataBase['width'].append(line_to_process.pop())
            dataBase['ymin'].append(line_to_process.pop())
            dataBase['xmin'].append(line_to_process.pop())
            dataBase['image'].append(image_name)
    return dataBase

def createImageDataBase(dataBase,imageDir):
    t1 = timeer()
    for line in range(0,len(dataBase)):
        t1.tic()
        lineProcess = dataBase.iloc[line]
        if os.path.exists("../labelImage/"+lineProcess[
            'busNumber']+"/"+lineProcess['image']):
            continue
        readImg = cv2.imread(imageDir+lineProcess['image'])
        val = readImg[int(lineProcess['ymin']):int(lineProcess['ymin']) + int(
            lineProcess['higth']),
              int(lineProcess['xmin']):int(lineProcess['xmin']) + int(
                  lineProcess['width']), 0:3]
        cv2.imwrite("../labelImage/"+lineProcess['busNumber']+"/"+lineProcess['image'],val)
        t1.toc()
    return

def createTraningDataBase():
    train_datagen = keras.preprocessing.image.ImageDataGenerator(rescale=1. /
                                                                         255, zoom_range=0.3,
                                       rotation_range=50,
                                       width_shift_range=0.2,
                                       height_shift_range=0.2, shear_range=0.2,
                                       horizontal_flip=True,
                                       fill_mode='nearest')

    #val_datagen = ImageDataGenerator(rescale=1. / 255)
    for label in range(1,7):
        labelDir = glob("../labelImage/"+str(label)+"/*.JPG")
        for file in labelDir:
            image = cv2.imread(file)
            image_generator  = train_datagen.flow(image,[label],batch_size=1)
            cat = [next(image_generator) for i in range(0, 10)]
            fig, ax = plt.subplots(1, 5, figsize=(16, 6))
            print('Labels:', [item[1][0] for item in cat])
            l = [ax[i].imshow(cat[i][0][0]) for i in range(0, 10)]

    return

def func_run():
    files = glob('../labelImage/*/*')
    label = []
    if(os.name == "nt"):
        slhash = "\\%s\\"
    else:
        slhash = "/%s/"
    label.append([fn for fn in files if slhash%('1') in fn])
    label.append([fn for fn in files if slhash%('2') in fn])
    label.append([fn for fn in files if slhash%('3') in fn])
    label.append([fn for fn in files if slhash%('4') in fn])
    label.append([fn for fn in files if slhash%('5') in fn])
    label.append([fn for fn in files if slhash%('6') in fn])
    train_files = glob('training_data/*')
    IMG_DIM = (128,128)
    for index in range(0,6):
        path = "../data/training/" + str(index + 1) + "/" + str(index + 1) + "-" + str(1) + ".jpg"
        if os.path.exists(path):
            continue
            images = label[index]
        train_imgs = [img_to_array(load_img(img, target_size=IMG_DIM)) for img in
                      images]
        train_imgs = np.array(train_imgs)
        train_labels = [fn.split('/')[1].split('.')[0].strip() for fn in
                        images]

        train_imgs = np.array(train_imgs)
        train_datagen = ImageDataGenerator(rescale=1. / 255, zoom_range=0.3,
                                           rotation_range=50,
                                           width_shift_range=0.2,
                                           height_shift_range=0.2, shear_range=0.2,
                                           horizontal_flip=True,
                                           fill_mode='nearest')

        val_datagen = ImageDataGenerator(rescale=1. / 255)

        img_id = 0
        bus_genertor = train_datagen.flow(train_imgs[0:img_id + 10],
                                           train_labels[img_id:img_id + 10],
                                           batch_size=30)
        counter = 0
        for runer in range(0,150):
            bus = [next(bus_genertor) for i in range(0, 10)]
            for indexR in range(0,10):
                counter = counter+1
                path = "../data/training/"+str(index+1)+"/"+str(index+1)+"-"+str(counter)+".jpg"
                print(path)
                cv2.imwrite(path,(bus[indexR][0][0]*255).astype(int))
    #fig, ax = plt.subplots(1, 5, figsize=(16, 6))
    #print('Labels:', [item[1][0] for item in cat])
    # = [ax[i].imshow(cat[i][0][0]) for i in range(0, 5)]
    exit()
    return

if __name__ == '__main__':
    i = 0
    val = {}
    while(i < len(sys.argv)):
        arg = sys.argv[i]
        if(arg == "-answer"):
            val['answer'] = sys.argv[i+1]
        elif(arg == "-busses"):
            val['busses'] = sys.argv[i + 1]
        elif(arg == "-annotation"):
            val['annotation'] = sys.argv[i + 1]
        i = i + 1
    dataBase = parseFile(val['annotation'])
    setUpDircoty("../labelImage/")
    setUpDircoty("../data/training/")
    dataBasePD = pd.DataFrame(dataBase)
    t = timeer()
    createImageDataBase(dataBasePD, val['busses'])
    func_run()
    createTraningDataBase()
    pd.DataFrame(dataBase).to_csv("result.csv")
    exit(1)

